import json 
import os
from subprocess import call

if __name__ == "__main__":

    paramfile = open("study-parameter.json",)
    study = json.load(paramfile)
    
    case_id = 0
    for resolution in study["resolutions"]:

        case_name = "case-%04d" % case_id 

        if (not os.path.exists(case_name)):
            os.mkdir(case_name)
        
        os.chdir(case_name)
        
        app_pathname = os.path.join(os.pardir, "build/myapp")
        print ("Running case %s: " % (case_id))
        call([app_pathname, str(resolution)])
        os.chdir(os.pardir)
        
        case_id = case_id + 1